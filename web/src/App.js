import React from 'react'
import { GlobalStyle } from 'components'
import { Home } from 'pages'

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Home />
    </>
  );
}

export default App
