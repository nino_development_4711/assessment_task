import GlobalStyle from './GlobalStyle'
import A from './A'
import H1 from './H1'
import LoadingIndicator from './LoadingIndicator'

export {
  GlobalStyle,
  A,
  H1,
  LoadingIndicator,
}