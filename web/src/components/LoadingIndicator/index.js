import styled, { keyframes } from 'styled-components'

const spin = keyframes`
  0% {
    transform: rotate(0deg); 
  }
  
  100% {
    transform: rotate(360deg);
  }
`;

// https://www.w3schools.com/howto/howto_css_loader.asp
const LoadingIndicator = styled.div`
  border: 4px solid #f3f3f3;
  border-top: 4px solid #3498db;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  animation: ${spin} 2s linear infinite;

  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
`
export default LoadingIndicator