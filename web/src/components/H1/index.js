import styled from 'styled-components'

const H1 = styled.h1`
  padding: 0;
  margin: 0;
  font-size: 1.6rem;
`

export default H1