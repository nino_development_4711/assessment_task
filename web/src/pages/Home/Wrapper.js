import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 20px;
  box-sizing: border-box;
  min-height: 100vh;
  background-color: #282c34;
  color: white;
`

export default Wrapper