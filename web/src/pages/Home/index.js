import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { H1, LoadingIndicator } from 'components'
import Wrapper from './Wrapper'
import Grid from './Grid'

function Home () {
  const [tables, setTables] = useState([])
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    (async () => {
      try {
        setLoading(true)
        const resTables = await fetch('http://localhost:4711/tables')
        const jsonTables = await resTables.json()
        setTables(jsonTables)

        const resData = await fetch('http://localhost:4711/')
        const jsonData = await resData.json()
        setData(jsonData)
      } catch (err) {
        console.error('Unable to fetch data', err)
      } finally {
        setLoading(false)
      }
    })()
  }, [])

  return (
    <Wrapper>
      {loading && <LoadingIndicator />}
      {!loading &&
        <div>
          <H1>CrateDB Tables</H1>
          <ul>
            {tables.map((t) => <li key={t.table_name}><code>{t.table_name}</code></li>)}
          </ul>

          <H1>ISS Data</H1>
          <Grid>
            <code>Timestamp</code>
            <code>Longitude</code>
            <code>Latitude</code>
          </Grid>
          {data
            .sort((d1, d2) => moment(d2.timestamp) - moment(d1.timestamp))
            .map((d) => {
              return (
                <Grid key={d.timestamp}>
                  <code>{moment(d.timestamp).format('DD.MM.YYYY HH:mm:ss')}</code>
                  <code>{d.position.x}</code>
                  <code>{d.position.y}</code>
                </Grid>
              )
            })}
        </div>}
    </Wrapper>
  )
}

export default Home;
