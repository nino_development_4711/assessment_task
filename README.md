# Assesment Task

## Prerequesites

* Docker (`docker-compose`)

## Run

`docker-compose up`

## Urls

* Crate: `localhost:4200`
* Server: `localhost:4711`
* Web: `localhost:3000`

## Notes

* This code is not production ready in any way. It is for presentational purposes only.
* The server example is based on the [Generate time series data using Node.js](https://crate.io/docs/crate/tutorials/en/latest/generate-time-series/node.html) tutorial.
* The React code is based on the official [CRA](https://create-react-app.dev/) boilerplate. It displays the tables from the crate db and the iss data from mentioned tutorial (manual refresh for update).
