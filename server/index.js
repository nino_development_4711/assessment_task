const express = require('express')
const cors = require('cors')
const axios = require('axios')
const { Pool } = require('pg')
const pool = new Pool({
  connectionString: 'postgresql://crate@crate:5432/doc',
})

const app = express()
app.use(cors())
app.listen(4711, function () {
  console.log('Express server listening...')
})

async function connect () {
  let retries = 10

  while (retries) {
    try {
      const client = await pool.connect()
      return client
    } catch (err) {
      console.log(err)
      retries -= 1
      console.log(`Retries left: ${retries}`)
      await new Promise(res => setTimeout(res, 5000))
    }
  }
}

;(async function () {
  const client = await connect()

  if (client) {
    // Register routes
    app.get('/', async function (req, res, next) {
      try {
        const { rows } = await client.query('SELECT * FROM iss')
        return res.json(rows)
      } catch (err) {
        return next(err)
      }
    })

    app.get('/tables', async function (req, res, next) {
      try {
        const { rows } = await client.query('SHOW TABLES')
        return res.json(rows)
      } catch (err) {
        return next(err)
      }
    })

    // Create table
    try {
      await client.query(`
        CREATE TABLE IF NOT EXISTS iss (
          timestamp TIMESTAMP GENERATED ALWAYS AS CURRENT_TIMESTAMP,
          position GEO_POINT
        )
      `)
      console.log('CREATE OK')
    } catch (err) {
      console.error('CREATE ERROR', err)
    }
    
    // Insert data
    while (true) {
      try {
        const res = await axios.get('http://api.open-notify.org/iss-now.json')
        const { longitude, latitude } = res.data.iss_position
        await client.query('INSERT INTO iss (position) VALUES (?)', [`POINT (${longitude} ${latitude})`])
        console.log('INSERT OK')
      } catch (err) {
        console.error('INSERT ERROR', err)
      }

      console.log('Sleeping for 3 seconds...')
      await new Promise((r) => {
        setTimeout(r, 3000)
      })
    }
  }
})()
